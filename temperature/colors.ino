const uint16_t colors[] = {
        matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255)
};

const uint16_t loopColors[] = {
  grid.Color(255, 255, 255), //white
  grid.Color(255, 0, 0), //fullRed,
  grid.Color(0, 255, 0), // fullGreen
  grid.Color(0, 0, 255) //fullBlue
};

uint8_t curColor = 0;


int intBrightness = BRIGHTNESSDAY; // the brightness of the clock (0 = off and 255 = 100%)

// a few colors
uint32_t colorWhite = grid.Color(255, 255, 255);
uint32_t colorBlack = grid.Color(0, 0, 0);
uint32_t colorRed = grid.Color(255, 0, 0);
uint32_t colorGreen = grid.Color(0, 255, 0);
uint32_t colorBlue = grid.Color(0, 0, 255);
uint32_t colorJGreen = grid.Color(50, 179, 30);


uint16_t getBlack(){
  return colorBlack;
}

uint16_t getGreen(){
  return colorGreen;
}
