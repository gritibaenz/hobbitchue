#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif


#include <RCSwitch.h>
RCSwitch mySwitch = RCSwitch();

#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 rtc;

int val;
#define tempPin 0
#define RGBPIN 7
#define N_LEDS 125 // 11 x 11 grid + 4 extra

#define BRIGHTNESSDAY 255 // full on
#define BRIGHTNESSPETRA 105 // petra mode
#define BRIGHTNESSNIGHT 75 // half on

//#include <SoftwareSerial.h>     // softserial library
//#define btRX 10                 // BlueTooth RX pin -> D10
//#define btTX 11                 // BlueTooth TX pin -> D11
//SoftwareSerial bSerial(btRX, btTX);  // RX, TX
//int btIn;                       // data in

// EEPROM
#include <EEPROM.h>
byte id_def = 1;              //special effects für Uhren, 1 = Michi, 2 = Flo, 3 = Wörmli
byte mode_def = 0;            // letzter Modus
byte color_def = 0;           // letzte Farbe
byte brighness_def = 0;           // Letzte Helligkeit
byte dst_def = 0;           // es ist jetzt Winterzeit = 0, es ist Sommerzeit = 1

byte s_id, s_mode, s_color, s_brightness, s_dst;
byte *set[] = {&s_id, &s_mode, &s_color, &s_brightness, &s_dst};
byte def[]  = {id_def, mode_def, color_def, brighness_def, dst_def};

// timeout
long interval = 1000;            // reset mode after this duration
long previousMillis = 0;          // old count
unsigned long currentMillis;      // current counter

// timeout
long intervalRainbow = 5;            // reset mode after this duration
uint16_t  iRb, jRb; //,

//MODES
#define mode_time 0				// ZEIT-Anzeige
#define mode_birthday 1				// HAPPY BIRTHDAY Anzeige
#define mode_temp 2

boolean modus = true;
int mode, curColor, curBright;

//TIME VARS
boolean IS_SUMMERTIME = false; //DEFINIERT OB BEI ZEITSETZUNG SOMMERZEIT WAR


Adafruit_NeoPixel grid = Adafruit_NeoPixel(N_LEDS, RGBPIN, NEO_GRB + NEO_KHZ800);

int intBrightness = BRIGHTNESSPETRA; // the brightness of the clock (0 = off and 255 = 100%)
const int brights[] = {160, 120, 60, 45, 30, 15, 0};
#define numBrights (sizeof(brights)/sizeof(int)) //array size  

const int modes[] = {0, 1, 2, 45, 30, 15, 0};
#define numBrights (sizeof(brights)/sizeof(int)) //array size  

const uint32_t colors[] = {
  grid.Color(255, 255, 255),
  grid.Color(255, 0, 0),
  grid.Color(0, 255, 0),
  grid.Color(0, 0, 255),
  grid.Color(224, 255, 255),
  grid.Color(0, 191, 255),
  grid.Color(255, 250, 205),
  grid.Color(240, 255, 240),
  grid.Color(255, 99, 71),
  grid.Color(0, 0, 0)
};
#define numColors (sizeof(colors)/sizeof(uint32_t)) //array size  

// a few colors
uint32_t colorWhite = grid.Color(255, 255, 255);
uint32_t colorBlack = grid.Color(0, 0, 0);
uint32_t colorRed = grid.Color(255, 0, 0);
uint32_t colorGreen = grid.Color(0, 255, 0);
uint32_t colorBlue = grid.Color(0, 0, 255);
uint32_t colorJGreen = grid.Color(50, 179, 30);

//WORTE DEFINIERT

int VIERMIN[] = {
  124, 123, 122, 121, -1
};
int DRUEMIN[] = {
  124, 122, 121, -1
};
int ZWOEIMIN[] = {
  121, 122, -1
};
int EIMIN[] = {
  121, -1
};
int ES[] = {
  120, 119, -1
};
int ISCH[] = { 117, 	116, 115, 114, -1 };
int FUEF[] = {
  112, 111, 110, -1
};
int VIERTU[] = {
  99, 100, 101, 102, 103, 104, -1
};
int ZAEAE[] = {
  106, 107, 108, -1
};
int ZWAENZG[] = {
  97, 96, 95, 94, 93, 92, -1
};
int VOR[] = {
  90, 89, 88, -1
};
int AB[] = {
  77, 78, -1
};
int HAUBI[] = {
  81, 82, 83, 84, 85, -1
};
int EIS[] = {
  76, 75, 74, -1
};
int ZWOEI[] = {
  73, 72, 71, 70, -1
};
int DRUE[] = {
  69, 68, 67, -1
};
int VIERI[] = {
  55, 56, 57, 58, 59, -1
};
int FUEFI[] = {
  61, 62, 63, 64, -1
};
int SAECHSI[] = {
  54, 53, 52, 51, 50, 49, -1
};
int SIBNI[] {
  48, 47, 46, 45, 44, -1
};
int ACHTI[] = {
  33, 34, 35, 36, 37, -1
};
int NUENI[] = {
  39, 40, 41, 42, -1
};
int ZAENI[] = {
  31, 30, 29, 28, -1
};
int OEUFI[] = {
  26, 25, 24, 23, -1
};
int ZWOEUFI[] = {
  13, 14, 15, 16, 17, 18, -1
};
int HOBBIT[] = {
  9, 8, 7, 6, 5, 4, -1
};
int BITCH[] = {
  6, 5, 4, 3, 2, -1
};
int CHUE[] = {
  3, 2, 1, 0, -1
};
int WOERMLI[] = {
  8, 7, 6, 5, 4, 3, -1
};

void setup()
{

  //Wire.begin();

  Serial.begin(9600);
  Serial.println(F("welcome to hobbitchue"));

  //bSerial.begin(115200);
  //delay(500);
  //bSerial.begin(38400);
  //bSerial.println(F("welcome to hobbitchue"));


  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
  }

  if (!rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    //rtc.adjust(DateTime(2017, 1, 1, 6, 00, 0));
  }
  //SIGI: DIESE ZEILE MIT // deaktivieren wenn Zeit stimmt #TIMEFIX
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  

  //rtc.adjust(DateTime(2018, 10, 28, 2, 59, 50)); // Test Zeitumstellung

  grid.begin();

  mySwitch.enableReceive(0);

  mode = 0;
  jRb = 0;
  // iRb = 0;

  restoreSettings();
  curColor = s_color;
  curBright = s_brightness;
  //s_dst = 0; // Überschreibt SOmmerzeit/winterzeit, 0 = aktuell ist winter


  //DELAY for FLASHING
  //delay(3000);
}

void loop()
{
  //Serial.println( F(__TIME__) );
  //setDefaults();
  doRCmagic();
  //doBlueTooth();

  currentMillis = millis();

  if ((currentMillis - previousMillis) > intervalRainbow && mode == 1) // time to update
  {
    previousMillis = millis();
    //doRCmagic();
    rainbow();
  }
  if ((currentMillis - previousMillis) > interval && mode == 0) // time to update
  {
    previousMillis = millis();
    lightUpRtcTime();
  }
}

void doRCmagic() {
  if (mySwitch.available()) {

    int value = mySwitch.getReceivedValue();

    if (value == 0) {
      Serial.print("Unknown encoding");
    } else {
      if (mySwitch.getReceivedValue() == 1361) {
        //A
        curColorLoop();

      }
      if (mySwitch.getReceivedValue() == 1364) {
        //B
        brightnessLoop();

      }
      if (mySwitch.getReceivedValue() == 4433) {
        //C
        resetColBright();
      }
      if (mySwitch.getReceivedValue() == 4436) {
        //D
        //brightnessLoop();
        modeLoop();
        //Serial.println("you pressed D - keine Funktion :-)" );
      }
      if (mySwitch.getReceivedValue() == 4436) {
        //E
        //brightnessLoop();
        Serial.println("you pressed E - keine Funktion :-)" );
      }
      if (mySwitch.getReceivedValue() == 4436) {
        //F
        //brightnessLoop();
        Serial.println("you pressed F - keine Funktion :-)" );
      }


      if (mySwitch.getReceivedValue() == 5396) {
        //MODE WECHSELN
        //if(++mode > 2) mode = 0;
        //modeLoop();
        if (s_dst == 1) {
          s_dst = 1;
        } else {
          s_dst = 0;
        }

      }

      Serial.print("Received ");
      Serial.print( mySwitch.getReceivedValue() );
      Serial.print(" / ");
      Serial.print( mySwitch.getReceivedBitlength() );
      Serial.print("bit ");
      Serial.print("Protocol: ");
      Serial.println( mySwitch.getReceivedProtocol() );
    }

    mySwitch.resetAvailable();
  }
}

void curColorLoop() {
  if (++curColor >= numColors) curColor = 0;
  Serial.print("Neue Farbe: ");
  Serial.println(curColor);

  s_color = curColor;
  saveSettings();
}

void brightnessLoop() {
  if (++curBright >= numBrights) curBright = 0;
  Serial.print("Neue Helle: ");
  Serial.println(curBright);

  s_brightness = curBright;
  saveSettings();

}

void resetColBright() {
  curColor = 0;
  curBright = 0;
  s_color = curColor;
  s_brightness = curBright;
  saveSettings();
}

void modeLoop() {

  if (++mode >= 2) mode = 0;

  Serial.print("Neuer Modus: ");
  Serial.println(mode);
}

void lightUpRtcTime() {

  doDSTmanipulation();


  DateTime now = rtc.now();

  grid.setBrightness(brights[curBright]);

  //clearen
  for (uint16_t iClear = 0; iClear < grid.numPixels(); iClear++) {
    grid.setPixelColor(iClear, colorBlack);
  }

  enlightenWord(ES, true);
  enlightenWord(ISCH, true);

  int modMin = now.minute() % 5;
  int printMin = now.minute() / 5;

  int tHour = now.hour();


  if (now.minute() > 24) {
    tHour++;
  }



  switch (modMin) {
    default: // 0min, off
      enlightenWord(VIERMIN, false);
      break;
    case 1:
      enlightenWord(EIMIN, true);
      break;
    case 2:
      enlightenWord(ZWOEIMIN, true);
      break;
    case 3:
      enlightenWord(DRUEMIN, true);
      break;
    case 4:
      enlightenWord(VIERMIN, true);
      break;
  }

  switch (printMin) {
    default:
      enlightenWord(BITCH, true);
      Serial.print(" BITCH: ");
      Serial.print(printMin, DEC);
      Serial.println();
      break;
    case 0:
    case 12:
      enlightenWord(FUEF, false);
      enlightenWord(AB, false);

      enlightenWord(ZAEAE, false);
      enlightenWord(ZWAENZG, false);
      enlightenWord(VOR, false);
      enlightenWord(VIERTU, false);
      enlightenWord(HAUBI, false);

      break;
    case 1:
      enlightenWord(FUEF, true);
      enlightenWord(AB, true);

      enlightenWord(ZAEAE, false);
      enlightenWord(ZWAENZG, false);
      enlightenWord(VOR, false);
      enlightenWord(VIERTU, false);
      enlightenWord(HAUBI, false);

      break;
    case 2:
      enlightenWord(ZAEAE, true);
      enlightenWord(AB, true);

      enlightenWord(FUEF, false);
      enlightenWord(ZWAENZG, false);
      enlightenWord(VOR, false);
      enlightenWord(VIERTU, false);
      enlightenWord(HAUBI, false);
      break;
    case 3:
      enlightenWord(VIERTU, true);
      enlightenWord(AB, true);

      enlightenWord(FUEF, false);
      enlightenWord(ZWAENZG, false);
      enlightenWord(VOR, false);
      enlightenWord(ZAEAE, false);
      enlightenWord(HAUBI, false);
      break;
    case 4:
      enlightenWord(ZWAENZG, true);
      enlightenWord(AB, true);

      enlightenWord(FUEF, false);
      enlightenWord(VIERTU, false);
      enlightenWord(VOR, false);
      enlightenWord(ZAEAE, false);
      enlightenWord(HAUBI, false);
      break;
    case 5:
      enlightenWord(FUEF, true);
      enlightenWord(VOR, true);
      enlightenWord(HAUBI, true);

      enlightenWord(VIERTU, false);
      enlightenWord(AB, false);
      enlightenWord(ZAEAE, false);
      enlightenWord(ZWAENZG, false);

      break;
    case 6:
      enlightenWord(HAUBI, true);

      enlightenWord(FUEF, false);
      enlightenWord(VOR, false);
      enlightenWord(VIERTU, false);
      enlightenWord(AB, false);
      enlightenWord(ZAEAE, false);
      enlightenWord(ZWAENZG, false);
      break;

    case 7:
      enlightenWord(FUEF, true);
      enlightenWord(HAUBI, true);
      enlightenWord(AB, true);

      enlightenWord(VOR, false);
      enlightenWord(VIERTU, false);
      enlightenWord(ZAEAE, false);
      enlightenWord(ZWAENZG, false);
      break;

    case 8:
      enlightenWord(ZWAENZG, true);
      enlightenWord(VOR, true);

      enlightenWord(FUEF, false);
      enlightenWord(HAUBI, false);
      enlightenWord(AB, false);
      enlightenWord(VIERTU, false);
      enlightenWord(ZAEAE, false);

      break;
    case 9:
      enlightenWord(VIERTU, true);
      enlightenWord(VOR, true);

      enlightenWord(ZWAENZG, false);
      enlightenWord(FUEF, false);
      enlightenWord(HAUBI, false);
      enlightenWord(AB, false);
      enlightenWord(ZAEAE, false);

      break;
    case 10:
      enlightenWord(ZAEAE, 1);
      enlightenWord(VOR, true);

      enlightenWord(VIERTU, 0);
      enlightenWord(ZWAENZG, false);
      enlightenWord(FUEF, false);
      enlightenWord(HAUBI, false);
      enlightenWord(AB, false);


      break;

    case 11:
      enlightenWord(FUEF, 1);
      enlightenWord(VOR, true);

      enlightenWord(VIERTU, 0);
      enlightenWord(ZWAENZG, false);
      enlightenWord(ZAEAE, 0);
      enlightenWord(HAUBI, false);
      enlightenWord(AB, false);

      break;
  }

  if (tHour > 12)
  {
    tHour = tHour - 12;
  }

  switch (tHour) {
    case 0:
    case 12:
      enlightenWord(EIS, 0); enlightenWord(ZWOEI, 0); enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0); enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0); enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0); enlightenWord(NUENI, 0); enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0); enlightenWord(ZWOEUFI, 1);
      break;
    case 1:
    case 13:
      enlightenWord(EIS, 1);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 2:
    case 14:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 2);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 3:
    case 15:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 3);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 4:
    case 16:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 4);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 5:
    case 17:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 5);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 6:
    case 18:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 6);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 7:
    case 19:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 7);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 8:
    case 20:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 8);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 9:
    case 21:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 9);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 10:
    case 22:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 1);
      enlightenWord(OEUFI, 0);
      enlightenWord(ZWOEUFI, 0);
      break;
    case 11:
    case 23:
      enlightenWord(EIS, 0);
      enlightenWord(ZWOEI, 0);
      enlightenWord(DRUE, 0);
      enlightenWord(VIERI, 0);
      enlightenWord(FUEFI, 0);
      enlightenWord(SAECHSI, 0);
      enlightenWord(SIBNI, 0);
      enlightenWord(ACHTI, 0);
      enlightenWord(NUENI, 0);
      enlightenWord(ZAENI, 0);
      enlightenWord(OEUFI, 1);
      enlightenWord(ZWOEUFI, 0);
      break;
  }

  if (
    (now.hour() == 13 && now.minute() == 37 && now.second() % 3 == 0) || 
    (now.hour() == 20  && now.minute() == 15 && now.second() % 3 == 0) || 
    (now.hour() == 22  && now.minute() == 22 && now.second() % 3 == 0) || 
    (now.hour() == 9  && now.minute() == 9 && now.second() % 3 == 0)) {
    if(id_def == 0){
      enlightenWord(BITCH, 1);
    }
    if(id_def == 1){
      enlightenWord(WOERMLI, 1);
    }
  }
  else {
    if(id_def == 0){
      enlightenWord(BITCH, 0);
    }
    if(id_def == 1){
      enlightenWord(WOERMLI, 0);
    }
  }

  //if (now.hour() == 20  && now.minute() == 15) {
  //  enlightenWord(BITCH, 1);
  //}
  //else {
  //  enlightenWord(BITCH, 0);
  //}


  grid.show();

}

void enlightenWord(int arr[], bool on) {

  for (int i = 0; i < (uint16_t)grid.numPixels() + 1; i++) {
    if (arr[i] == -1) {
      // grid.show();
      break;
    }
    else {
      grid.setPixelColor(arr[i], getFarb(on));
    }
  }
}

uint32_t getFarb(boolean on) {
  if (on) {
    return colors[curColor];
  }
  else {
    return colorBlack;
  }
}
// ********************************************************************************** //
//                              BLUTUTH
// ********************************************************************************** //
/*
  void doBlueTooth() {
  if (bSerial.available()) {                    // if BlueTood data waiting
    btIn = bSerial.read();                      // read it

    if (btIn == 'a') {                          // if mode increment
      curColorLoop();
    }
    else if (btIn == 'b') {           // reset to normal mode
      brightnessLoop();
    }
    else if (btIn == 'c') {
      resetColBright();
    }
    else if (btIn == 'd') {
      //resetColBright();
    }
    else if (btIn == 'e') {
      //resetColBright();
    }
    else if (btIn == 'f') {
      //resetColBright();
    }
    //Serial.print("got bt: ");
    //Serial.println(btIn);
  }

  }*/
// ********************************************************************************** //
//                              SOMMERZEIT
// ********************************************************************************** //

void doDSTmanipulation() {

  DateTime now = rtc.now();

  //if(s_id == 1) {
    //Serial.println(*set[0]);
  //}

  if (now.hour() == 6 && now.minute() == 0 && now.second() == 0) { // run daily at exactly 6:01 A.M.
    
    delay(3000); // wait 8.0 seconds (that's 6.0 secs drift plus 1 second then reset seconds to 9:01:01 see below)
 
    Serial.println("timefix adjust");
    rtc.adjust(DateTime(now.unixtime() + 1));
  }

  bool bDst = IsDst(now.day(), now.month(), now.dayOfTheWeek());
  /*Serial.print("isDST isch ");
    Serial.print(bDst);
    Serial.print(" ");
    Serial.println(now.second());*/
  if (bDst == true && s_dst == 0 && now.hour() >= 2) {

    s_dst = 1;
    rtc.adjust(DateTime(now.unixtime() + 3600));
    saveSettings();
  }
  else if (bDst == false && s_dst == 1 && now.hour() >= 3) {
    s_dst = 0;
    rtc.adjust(DateTime(now.unixtime() - 3600));
    saveSettings();
  }


}

bool IsDst(int day, int month, int dow)
{
  if (month < 3 || month > 10) return false;
  if (month > 3 && month < 10) return true;

  int previousSunday = day - dow;

  if (month == 3) return previousSunday >= 25;
  if (month == 10) return previousSunday < 25;

  return false; // this line never gonna happend
}

// ********************************************************************************** //
//                              FUN RAINBOW
// ********************************************************************************** //
void rainbow() {


  if ( ++jRb > 1024) {
    jRb = 0;
    modeLoop();
  };

  //if ( ++iRb > grid.numPixels()) {
  //   iRb = 0;
  //   modeLoop();
  //};
  //grid.setPixelColor(iRb, Wheel((iRb + jRb) & 255));

  for (iRb = 0; iRb < grid.numPixels(); iRb++) {
    doRCmagic();
    grid.setPixelColor(iRb, Wheel((iRb + jRb) & 255));
  }

  grid.show();

  // delay(wait);
}
// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return grid.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return grid.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return grid.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
// ********************************************************************************** //
//                              EEPROM MANAGEMENT
// ********************************************************************************** //

void saveSettings()    {
  for (int addr = 0; addr < sizeof(set) / 2; addr++) {
    EEPROM.write(addr, *set[addr]);  // save
  }
}

void restoreSettings() {               // restore settings
  boolean doDef = true;                // if no settings have been previously stored, flag is true
  for (int addr = 0; addr < sizeof(set) / 2; addr++) {
    *set[addr] = EEPROM.read(addr);    // recover EEPROM values
    if ( EEPROM.read(addr) != 0xFF ) doDef = false; // if at least one setting is not 0xFF then reset flag
  }
  Serial.println("restoreSettings");
 
  if (doDef) setDefaults();           // however, if no settings have been set then set defaults.
}

void setDefaults()     {
  Serial.println("defaults set");
  for (int addr = 0; addr < sizeof(set) / 2; addr++) {
    *set[addr] = def[addr];  // set defaults
  }
}

